import time
import clr
import PublicDef

clr.AddReference('hEmailDll')
from hEmailDll import *
instance = EmailClass()


def Email_Geturl(request):
    if request.method == 'POST':
        name = str(request.POST.get("name"))
        timestamp = str(request.POST.get("timestamp"))
        if (int(time.time() * 1000) - int(timestamp)) > 10000 or (int(timestamp) - int(time.time() * 1000)) > 10000:
            return PublicDef.ToJsonRep({"msg": "会话超时，请检查系统时间是否与北京时间一致！"})
        Sign_1 = str(request.POST.get("sign"))
        Sign_2 = PublicDef.Sign(name, timestamp)
        if Sign_1 != Sign_2:
            return PublicDef.ToJsonRep({"msg": "无效会话！"})
        if name != "geturl":
            return PublicDef.ToJsonRep({"msg": "参数错误！"})
        ret = instance.getUrl()
        return PublicDef.ToJsonRep({"msg": ret})
    else:
        return PublicDef.ToJsonRep({"msg": "Only Post"})
