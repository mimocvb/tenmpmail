from django.conf.urls import url
from Email_Create import views as Create_View
from Email_Delete import views as Delete_View
from Email_Select import views as Select_View
from Email_Geturl import views as Geturl_View
from Email_Downld import views as Downld_View
from Email_Remail import views as Remail_View
from Email_Index import views as Index_View

urlpatterns = [
    url(r'^api/email/create', Create_View.Email_Create),
    url(r'^api/email/delete', Delete_View.Email_Delete),
    url(r'^api/email/select', Select_View.Email_Select),
    url(r'^api/email/geturl', Geturl_View.Email_Geturl),
    url(r'^api/email/getapk', Downld_View.Email_Downld),
    url(r'^api/email/remail', Remail_View.Email_Reamil),
    url(r'^', Index_View.Email_Index),
]
