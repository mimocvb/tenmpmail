import time
import clr
import PublicDef

clr.AddReference('hEmailDll')
from hEmailDll import *
instance = EmailClass()


def Email_Delete(request):
    if request.method == 'POST':
        name = str(request.POST.get("name"))
        domain = str(request.POST.get("domain"))
        timestamp = str(request.POST.get("timestamp"))
        if (int(time.time() * 1000) - int(timestamp)) > 10000 or (int(timestamp) - int(time.time() * 1000)) > 10000:
            return PublicDef.ToJsonRep({"msg": "会话超时，请检查系统时间是否与北京时间一致！"})
        Sign_1 = str(request.POST.get("sign"))
        Sign_2 = PublicDef.Sign(name, timestamp)
        if Sign_1 != Sign_2:
            return PublicDef.ToJsonRep({"msg": "无效会话！"})
        if name == "mimo" or name == "chenpur":
            return PublicDef.ToJsonRep({"msg": "您没有权限删除该账号！"})
        ret = instance.delUser(name, domain)
        if ret.find("无效索引") != -1:
            ret = "该账号不存在"
        return PublicDef.ToJsonRep({"msg": ret})
    else:
        return PublicDef.ToJsonRep({"msg": "Only Post"})
