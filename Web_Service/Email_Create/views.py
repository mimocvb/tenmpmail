import clr
import time
import PublicDef

clr.AddReference('hEmailDll')
from hEmailDll import *
instance = EmailClass()


def Email_Create(request):
    if request.method == 'POST':
        name = str(request.POST.get("name"))
        domain = str(request.POST.get("domain"))
        timestamp = str(request.POST.get("timestamp"))
        if (int(time.time() * 1000) - int(timestamp)) > 10000 or (int(timestamp) - int(time.time() * 1000)) > 10000:
            return PublicDef.ToJsonRep({"msg": "会话超时，请检查系统时间是否与北京时间一致！"})
        Sign_1 = str(request.POST.get("sign"))
        Sign_2 = PublicDef.Sign(name, timestamp)
        if Sign_1 != Sign_2:
            return PublicDef.ToJsonRep({"msg": "无效会话！"})
        if name == "mimo" or name == "chenpur":
            return PublicDef.ToJsonRep({"msg": "您没有权限创建该账号！"})
        ret = instance.addUser(name, domain)
        if ret.find("Another object with the same name already exists in this domain") != -1:
            ret = "该账号已存在"
        if ret.find("The account address is not a valid email address") != -1:
            ret = "邮件名称不合法"
        return PublicDef.ToJsonRep({"msg": ret})
    else:
        return PublicDef.ToJsonRep({"msg": "Only Post"})
