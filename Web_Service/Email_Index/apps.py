from django.apps import AppConfig


class EmailIndexConfig(AppConfig):
    name = 'Email_Index'
