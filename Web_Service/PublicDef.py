import hashlib

from django.http import JsonResponse

appKey = "test"
Vension = "1.2.1"


def Sign(name, timestamp):
    global appKey
    md5 = hashlib.md5()
    strName = appKey + "{\"name\":\"" + name + "\",\"timestamp\":" + timestamp + ",\"appkey\":\"" + appKey + "\"}" + appKey
    md5.update(strName.encode(encoding='utf-8'))
    return md5.hexdigest()


def ToJsonRep(objs):
    response = JsonResponse(objs, json_dumps_params={'ensure_ascii': False})
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "GET,POST"
    return response
