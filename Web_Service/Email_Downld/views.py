import time

from django.http import FileResponse

import PublicDef

# Create your views here.
def Email_Downld(request):
    if request.method == 'GET':
        file = open("DownLoadFile/Temp-Mail.apk", 'rb')
        response = FileResponse(file)
        # response["Content-Length"] = len(file.read())
        response['Content-Disposition'] = 'attachment;filename="Temp-Mail.apk"' # 文件名称和类型
        return response
    else:
        name = str(request.POST.get("name"))
        timestamp = str(request.POST.get("timestamp"))
        if (int(time.time() * 1000) - int(timestamp)) > 10000 or (int(timestamp) - int(time.time() * 1000)) > 10000:
            return PublicDef.ToJsonRep({"msg": "会话超时，请检查系统时间是否与北京时间一致！"})
        Sign_1 = str(request.POST.get("sign"))
        Sign_2 = PublicDef.Sign(name, timestamp)
        if Sign_1 != Sign_2:
            return PublicDef.ToJsonRep({"msg": "无效会话！"})
        return PublicDef.ToJsonRep({"msg": "Success", "ven": PublicDef.Vension})
