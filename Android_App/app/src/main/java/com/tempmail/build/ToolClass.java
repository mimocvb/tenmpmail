package com.tempmail.build;

import android.content.Context;
import android.widget.Toast;
import org.json.JSONObject;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ToolClass{

    private final Context context;
    private final String FileName;

    public ToolClass(String Name, Context c){
        FileName = Name;
        context = c;
    }

    public String getProperties(String key) {
        try {
            String result;
            if(fileIsExists()){
                FileInputStream fin = context.openFileInput(FileName);
                int lenght = fin.available();
                byte[] buffer = new byte[lenght];
                fin.read(buffer);
                result = new String(buffer);
                JSONObject jo = new JSONObject(result);
                return jo.get(key).toString();
            }
            else {
                return null;
            }
        } catch (Exception e) {
            Toast.makeText(context, "读取配置文件失败！", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    public void setProperties(String key, String value) {
        try{
            JSONObject jo;

            if(fileIsExists()){
                String result;
                FileInputStream fin = context.openFileInput(FileName);
                int lenght = fin.available();
                byte[] buffer = new byte[lenght];
                fin.read(buffer);
                result = new String(buffer);
                jo = new JSONObject(result);
            }
            else{
                jo = new JSONObject();
            }

            jo.put(key, value);

            FileOutputStream fout = context.openFileOutput(FileName, Context.MODE_PRIVATE);
            fout.write(jo.toString().getBytes());
            fout.close();
        } catch (Exception ex){
            Toast.makeText(context, "写入配置文件失败！", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean fileIsExists()
    {
        try
        {
            context.openFileInput(FileName);
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }
}
