package com.tempmail.build;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class MailView extends AppCompatActivity {

    private long mExitTime;
    private String name, domain;
    protected boolean useThemestatusBarColor = false;
    protected boolean useStatusBarColor = true;
    private FloatingActionButton DelBtn;
    private final HttpClass hc = new HttpClass("https://temp.gugugun.com");
    private final ArrayList<HashMap<String, String>> deviceList = new ArrayList<>();
    private final String[] Listkeys = {"title", "from", "value"};
    private ListView listView;
    private TextView VisMail, MailText;
    private TableLayout MailtextLayout;
    private SimpleAdapter simpleAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final ToolClass Config = new ToolClass("Config.conf", this);

    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                simpleAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                if(deviceList.size() > 0){
                    VisMail.setVisibility(View.INVISIBLE);
                }
                else {
                    VisMail.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_out,R.anim.fade_in); //淡入
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_view);
        setStatusBar();

        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        domain = bundle.getString("domain");
        swipeRefreshLayout = findViewById(R.id.RefList);

        new Thread(() -> {
            Looper.prepare();
            selectMail(); //先获取一次邮件
            Looper.loop();
        }).start();

        listView = findViewById(R.id.MailList);
        MailText = findViewById(R.id.Mailtext);
        DelBtn = findViewById(R.id.DelMail);
        VisMail = findViewById(R.id.VisText);
        MailtextLayout = findViewById(R.id.MailtextLayout);

        MailText.setText("当前邮箱：" + name + domain);

        MailtextLayout.setOnClickListener(v -> {
            ClipboardManager cmb = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
            cmb.setText(name + domain);
            Toast.makeText(this, "已复制邮箱", Toast.LENGTH_SHORT).show();
        });

        DelBtn.setOnClickListener(v -> {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "再按一次销毁邮箱", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();
            } else {
                Config.setProperties("name", "");
                Config.setProperties("domain", "");
                startActivity(new Intent(MailView.this, MainActivity.class));
                finish();
            }
        });

        int[] ids = {android.R.id.text1, android.R.id.text2};
        simpleAdapter = new SimpleAdapter(this, deviceList, android.R.layout.simple_list_item_2, Listkeys, ids) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                // 设置第一行字体颜色和大小
                TextView text1 = view.findViewById(android.R.id.text1);
                text1.setTextColor(Color.parseColor("#7C7C7C"));
                text1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

                // 第一行字体加粗
                TextPaint tp = text1.getPaint();
                tp.setFakeBoldText(true);

                // 设置第二行字体颜色和大小
                TextView text2 = view.findViewById(android.R.id.text2);
                text2.setTextColor(Color.parseColor("#9F9F9F"));
                text2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

                return view;
            }
        };
        listView.setAdapter(simpleAdapter);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            deviceList.clear();
            simpleAdapter.notifyDataSetChanged();
            new Thread(() -> {
                Looper.prepare();
                selectMail();
                Looper.loop();
            }).start();
        });

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            HashMap<String, String> map = (HashMap<String, String>) adapterView.getItemAtPosition(i);

            Intent intent = new Intent(MailView.this, ValueView.class);
            intent.putExtra("value", map.get("value"));
            intent.putExtra("title", map.get("title"));
            intent.putExtra("from", map.get("from"));
            startActivity(intent);
        });
    }

    private void selectMail(){
        try{
            long timeStamp = System.currentTimeMillis();
            String sign = hc.GetSign(name, String.valueOf(timeStamp));
            String rets = hc.HttpPost("/api/email/select", "name=" + name + "&timestamp=" + timeStamp + "&sign=" + sign + "&domain=" + domain);
            if(rets == null || rets.equals("")){
                MessageBox("Error", "获取邮件失败，请重试！");
                return;
            }
            JSONObject jo = new JSONObject(rets);
            JSONArray data = (JSONArray) jo.get("data");

            if(!Objects.equals(jo.get("code"), "0")){
                MessageBox("Error", Objects.requireNonNull(jo.get("msg")).toString());
            }
            else {
                HashMap<String, String> info;
                for (int i = 0; i < data.length(); i++){
                    info = new HashMap<>();
                    JSONObject tmpJ = (JSONObject) data.get(i);
                    info.put("title", tmpJ.get("Title").toString());
                    info.put("from", tmpJ.get("From").toString());
                    info.put("value", tmpJ.get("Value").toString());
                    deviceList.add(info);
                }

                Message msg = new Message();
                msg.what = 1;
                handler.sendMessage(msg);
            }
        }
        catch (Exception ex){
            MessageBox("Error", "错误：" + ex.getMessage() + "\n位置：" + ex.getStackTrace()[0]);
        }
    }

    private void MessageBox(String Title, String Value){
        AlertDialog.Builder builder = new AlertDialog.Builder(MailView.this);
        builder.setTitle(Title);
        builder.setMessage(Value);
        builder.setPositiveButton("确定", null);
        builder.show();
    }

    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);

            if (useThemestatusBarColor) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.white));
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        } else {
            Toast.makeText(this, "低于4.4的android系统版本不存在沉浸式状态栏", Toast.LENGTH_SHORT).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && useStatusBarColor) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    //点击两次返回退出app   System.currentTimeMillis()系统当前时间
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setProperties(Context c, String e, String d) {
        try{
            FileOutputStream fout = openFileOutput("Config.conf", MODE_PRIVATE);//获得FileOutputStream
            //将要写入的字符串转换为byte数组
            fout.write(("{\"email\":\"" + e + "\",\"domain\":\"" + d + "\"}").getBytes());//将byte数组写入文件
            fout.close();//关闭文件输出流
        } catch (Exception ex){
            Toast.makeText(c, "写入配置文件失败！" + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}