package com.tempmail.build;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.GnssAntennaInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private AlertDialog Loading;
    private long mExitTime;
    protected boolean useThemestatusBarColor = false;
    protected boolean useStatusBarColor = true;
    private Button Btn;
    private TextView VenText;
    private Spinner urlList;
    private EditText MailInput;
    private final HttpClass hc = new HttpClass("https://temp.gugugun.com");
    private final List<String> data_list = new ArrayList<>();
    private final String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final List<String> mPermissionList = new ArrayList<>();
    private final ToolClass Config = new ToolClass("Config.conf", this);

    // private ImageView welcomeImg = null;
    private static final int PERMISSION_REQUEST = 1;

    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                ArrayAdapter<String> arr_adapter= new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, data_list);
                //设置样式
                arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //加载适配器
                urlList.setAdapter(arr_adapter);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_out,R.anim.fade_in); //淡入

        checkPermission();

        new Thread(() -> {
            Looper.prepare();
            Loading = MessageBoxD("提示", "正在连接服务器...");
            Looper.loop();
        }).start();

        new Thread(() -> {
            Looper.prepare();
            String ven = getVension();
            if(ven.equals("") || ven == null){
                Loading.dismiss();
                MessageBox("提示", "验证版本信息失败，请检测网络是否通畅！", false, (v, c) -> {
                    finish();
                });
            }
            else {
                if(!ven.equals(hc.Vension)){
                    MessageBox("提示", "新版本已发布，点击确定前往下载！", false, (v, c) -> {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://temp.gugugun.com/api/email/getapk"));
//                        intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
//                        startActivity(intent);
                        startActivity(Intent.createChooser(intent,"请选择浏览器"));
                        finish();
                    });
                }
                else {
                    String e = Config.getProperties("name");
                    String d = Config.getProperties("domain");

                    if(e != null && d != null && e.length() > 0 && d.length() > 0){
                        Intent intent = new Intent(MainActivity.this, MailView.class);
                        intent.putExtra("name", e);
                        intent.putExtra("domain", d);
                        startActivity(intent);
                        finish();
                    }

                    Loading.dismiss();
                }
            }
            Looper.loop();
        }).start();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setStatusBar();

        urlList = findViewById(R.id.urlList);
        Btn = findViewById(R.id.GetMail);
        MailInput = findViewById(R.id.MailInput);
        VenText = findViewById(R.id.VenText);

        VenText.setText("Vension " + hc.Vension);

        new Thread(() -> {
            Looper.prepare();
            getUrlList();
            Looper.loop();
        }).start();

        Btn.setOnClickListener(v -> new Thread(() -> {
            Looper.prepare();
            getMail();
            Looper.loop();

        }).start());

        urlList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tv = (TextView)view;
                tv.setTextColor(getResources().getColor(R.color.List));    //设置颜色

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private String getVension(){
        try{
            long timeStamp = System.currentTimeMillis();
            String sign = hc.GetSign("getupdate", String.valueOf(timeStamp));
            String rets = hc.HttpPost("/api/email/getapk", "name=getupdate&timestamp=" + timeStamp + "&sign=" + sign);

            if(rets == null || rets.equals("")){
                throw new Exception("获取版本号异常");
            }

            JSONObject jo = new JSONObject(rets);

            if(!jo.get("msg").toString().equals("Success")){
                throw new Exception(jo.get("msg").toString());
            }

            return jo.get("ven").toString();
        }
        catch (Exception ex){
            MessageBox("Error", "获取更新信息失败！\nMsg：" + ex.getMessage(), false, (v, c) -> finish());
        }

        return "";
    }

    private void getMail(){
        try{
            long timeStamp = System.currentTimeMillis();
            String mailAdd = MailInput.getText().toString();
            String domain = urlList.getSelectedItem().toString();
            String sign = hc.GetSign(mailAdd, String.valueOf(timeStamp));
            String rets = hc.HttpPost("/api/email/create", "name=" + mailAdd + "&timestamp=" + timeStamp + "&sign=" + sign + "&domain=" + domain);
            if(rets == null || rets.equals("")){
                MessageBox("Error", "申请临时邮箱失败！");
                return;
            }

            Map<String, Object> JsonData = hc.jsonToMap(rets);
            String msg = Objects.requireNonNull(JsonData.get("msg")).toString();
            if(!msg.equals("Success") && !msg.equals("该账号已存在")){
                MessageBox("Error", Objects.requireNonNull(JsonData.get("msg")).toString());
            }
            else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                Intent intent = new Intent(MainActivity.this, MailView.class);
                intent.putExtra("name", mailAdd);
                intent.putExtra("domain", domain);
                startActivity(intent);

                Config.setProperties("name", mailAdd);
                Config.setProperties("domain", domain);

                finish();
            }
        }
        catch (Exception ex){
            MessageBox("Error", "错误：" + ex.getMessage() + "\n位置：" + ex.getStackTrace()[0]);
        }
    }

    private void getUrlList(){
        try{
            long timeStamp = System.currentTimeMillis();
            String sign = hc.GetSign("geturl", String.valueOf(timeStamp));
            String rets = hc.HttpPost("/api/email/geturl", "name=geturl&timestamp=" + timeStamp + "&sign=" + sign);

            if(rets == null || rets.equals("")){
                MessageBox("Error", "获取Url地址失败！");
                return;
            }

            Map<String, Object> JsonData = hc.jsonToMap(rets);
            String[] Msg = Objects.requireNonNull(JsonData.get("msg")).toString().split(",");

            if(!Msg[0].equals("Success")){
                MessageBox("Error", "获取Url地址失败！");
                return;
            }

            for (String str: Msg) {
                if(str.equals("Success")){
                    continue;
                }
                data_list.add("@" + str);
            }

            Message msg = new Message();
            msg.what = 1;
            handler.sendMessage(msg);
        }
        catch (Exception ex){
            MessageBox("Error", "错误：" + ex.getMessage() + "\n位置：" + ex.getStackTrace()[0]);
        }
    }

    private void MessageBox(String Title, String Value){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(Title);
        builder.setMessage(Value);
        builder.setPositiveButton("确定", null);
        builder.show();
    }

    private void MessageBox(String Title, String Value, DialogInterface.OnClickListener obj){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(Title);
        builder.setMessage(Value);
        builder.setPositiveButton("确定", obj);
        builder.setCancelable(false);
        builder.show();
    }

    private void MessageBox(String Title, String Value, Boolean Enable, DialogInterface.OnClickListener obj){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(Title);
        builder.setMessage(Value);
        builder.setPositiveButton("确定", obj);
        builder.setCancelable(Enable);
        builder.show();
    }

    private AlertDialog MessageBoxD(String Title, String Value){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(Title);
        builder.setMessage(Value);
        builder.setCancelable(false);
        return builder.show();
    }

    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);

            if (useThemestatusBarColor) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.white));
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        } else {
            Toast.makeText(this, "低于4.4的android系统版本不存在沉浸式状态栏", Toast.LENGTH_SHORT).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && useStatusBarColor) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    //点击两次返回退出app   System.currentTimeMillis()系统当前时间
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkPermission() {
        mPermissionList.clear();

        //判断哪些权限未授予
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }
        /**
         * 判断是否为空
         */
        if (mPermissionList.isEmpty()) {//未授予的权限为空，表示都授予了

        } else {//请求权限方法
            String[] permissions = mPermissionList.toArray(new String[mPermissionList.size()]);//将List转为数组
            ActivityCompat.requestPermissions(MainActivity.this, permissions, PERMISSION_REQUEST);
        }
    }

    /**
     * 响应授权
     * 这里不管用户是否拒绝，都进入首页，不再重复申请权限
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST:
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
}